package baseDeDatos;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConexionSingleton {
	private static Connection conn = null;

	private ConexionSingleton() {

	}

	public static Connection getInstance() {
		try {
			if (conn == null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				// Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/proyectocontrolderecetas?useTimezone=true&serverTimezone=UTC",
						"root", "");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return conn;
	}
}
