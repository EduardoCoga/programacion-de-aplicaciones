package baseDeDatos;
import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
	public String nombreBD = "proyectocontrolderecetas";
	public String url = 
			"jdbc:mysql://localhost:3306/"+nombreBD
			+ "?useTimezone=true&serverTimezone=UTC";
	public String usuario = "root";
	public String password = "";

	public Connection conectarBD() {
		Connection conexion = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			//Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(url, usuario, password);
		} catch (Exception e) {
			System.out.println(e);
		}
		return conexion;
	}
}
