package GUIs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sound.midi.MidiDevice.Info;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import adapterPlatillo.Adapter;
import adapterPlatillo.Platillos;
import instrucciones.Platillo;

public class GUIPlatillo extends GUI {

	JInternalFrame framePlatillos;
	JPanel panelInputs;
	JPanel panelControl;
	JButton buttonAgregar;
	JButton buttonEliminar;
	JButton buttonModificar;
	JButton buttonConsultar;
	JTextField nombre;
	JComboBox<String> tipoPlatillo;
	JComboBox<String> tipoPreparacion;
	JComboBox<String> tipoComida;
	String respaldo;

	public void crearFramePlatillo() {
		// JInternalFrame
		Font fuente = new Font("Arial", 3, 20);
		JLabel info;

		// InternalFrame Platillos
		framePlatillos = new JInternalFrame("Platillos", true, true, true);
		framePlatillos.setBounds(0, 0, 700, 400);
		framePlatillos.setLayout(new FlowLayout(FlowLayout.CENTER));
		framePlatillos.setBackground(new Color(60, 90, 153));
		// framePlatillos.setLayout(new FlowLayout());

		// Panel de entrada
		panelInputs = new JPanel();
		panelInputs.setBackground(new Color(60, 90, 153));
		panelInputs.setLayout(new GridLayout(4, 2));

		// Primer dato
		info = new JLabel();
		info.setText("Nombre del platillo: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		nombre = new JTextField();
		nombre.setFont(fuente);
		nombre.setBackground(new Color(255, 255, 255));
		nombre.setForeground(new Color(0, 0, 0));
		nombre.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int k = (int) e.getKeyChar();
				 if (k > 47 && k < 58) {
			            e.setKeyChar((char) KeyEvent.VK_CLEAR);
			            JOptionPane.showMessageDialog(null, "No puede ingresar numeros", "Error Datos", JOptionPane.ERROR_MESSAGE);
			        }
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub				
			}
		});
		panelInputs.add(info);
		panelInputs.add(nombre);

		// Segundo Dato
		info = new JLabel();
		info.setText("Tipo de platillo: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		panelInputs.add(info);
		tipoPlatillo = new JComboBox<String>();
		tipoPlatillo.addItem("Tipico");
		tipoPlatillo.addItem("Afrodisiaco");
		tipoPlatillo.addItem("Postre");
		tipoPlatillo.addItem("Ensalada");
		tipoPlatillo.addItem("Panesillos");
		tipoPlatillo.addItem("Salsas");
		tipoPlatillo.addItem("Bebidas");
		tipoPlatillo.setBackground(new Color(255, 255, 255));
		tipoPlatillo.setForeground(new Color(0, 0, 0));
		tipoPlatillo.setFont(fuente);
		panelInputs.add(tipoPlatillo);

		// Tercer Dato
		info = new JLabel();
		info.setText("Tipo de preparaci�n: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		panelInputs.add(info);
		tipoPreparacion = new JComboBox<String>();
		tipoPreparacion.addItem("Horneado");
		tipoPreparacion.addItem("Frito");
		tipoPreparacion.addItem("Asado");
		tipoPreparacion.addItem("Guisado");
		tipoPreparacion.setBackground(new Color(255, 255, 255));
		tipoPreparacion.setForeground(new Color(0, 0, 0));
		tipoPreparacion.setFont(fuente);
		panelInputs.add(tipoPreparacion);

		// Cuarto DAto
		info = new JLabel();
		info.setText("Tipo de comida: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		panelInputs.add(info);
		tipoComida = new JComboBox<String>();
		tipoComida.addItem("Desayuno");
		tipoComida.addItem("Comida");
		tipoComida.addItem("Cena");
		tipoComida.setFont(fuente);
		tipoComida.setBackground(new Color(255, 255, 255));
		tipoComida.setForeground(new Color(0, 0, 0));
		panelInputs.add(tipoComida);

		panelInputs.setPreferredSize(new Dimension(500, 200));

		// Panel de control
		panelControl = new JPanel();
		panelControl.setFont(fuente);
		panelControl.setBackground(new Color(60, 90, 153));
		panelControl.setPreferredSize(new Dimension(500, 100));
		panelControl.setLayout(new FlowLayout(SwingConstants.VERTICAL, 10, 10));

		// Crear botones
		buttonAgregar = new JButton("Agregar");
		buttonConsultar = new JButton("Consultar");
		buttonEliminar = new JButton("Eliminar");
		buttonModificar = new JButton("Modificar");

		// Color botones
		buttonAgregar.setBackground(new Color(60, 90, 153));

		// Tipo de fuente y color de los BOTONES
		buttonAgregar.setFont(fuente);
		buttonAgregar.setBackground(new Color(255, 255, 255));
		buttonAgregar.setForeground(new Color(0, 0, 0));
		buttonConsultar.setFont(fuente);
		buttonConsultar.setBackground(new Color(255, 255, 255));
		buttonConsultar.setForeground(new Color(0, 0, 0));
		buttonModificar.setFont(fuente);
		buttonModificar.setBackground(new Color(255, 255, 255));
		buttonModificar.setForeground(new Color(0, 0, 0));
		buttonEliminar.setFont(fuente);
		buttonEliminar.setForeground(new Color(0, 0, 0));
		buttonEliminar.setBackground(new Color(255, 255, 255));

		// Action listener
		buttonAgregar.addActionListener(this);
		buttonConsultar.addActionListener(this);
		buttonModificar.addActionListener(this);
		buttonEliminar.addActionListener(this);

		// Agregar al panel
		panelControl.add(buttonAgregar);
		panelControl.add(buttonConsultar);
		panelControl.add(buttonModificar);
		panelControl.add(buttonEliminar);

		// Agregar al DesktopPane
		framePlatillos.add(panelInputs);
		framePlatillos.add(panelControl);
		//Alturas minimas y maximas
		framePlatillos.setMinimumSize(new Dimension(700,350));
		framePlatillos.setMaximumSize(new Dimension(1100, 400));
		desktopPane.add(framePlatillos);
		framePlatillos.show();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		Object source = evt.getSource();
		Platillo recibir = new Platillo();
		String platilloBuscar;
		ResultSet rs;
		if (source == buttonAgregar) {
			if(!nombre.getText().isEmpty()){
				if(tipoComida.getSelectedItem() == "Desayuno"){
					Adapter ad = new Adapter();
					ad.registrarPlatillo(nombre.getText(), String.valueOf(tipoPlatillo.getSelectedItem()),
						String.valueOf(tipoPreparacion.getSelectedItem()), String.valueOf(tipoComida.getSelectedItem()));
				}else{
					Platillos pl = new Platillos();
					pl.registrarPlatillo(nombre.getText(), String.valueOf(tipoPlatillo.getSelectedItem()),
						String.valueOf(tipoPreparacion.getSelectedItem()), String.valueOf(tipoComida.getSelectedItem()));
				}
			}else{
				JOptionPane.showMessageDialog(null, "No se puede continuar hasta que complete el formulario", "Revise los datos",
						JOptionPane.ERROR_MESSAGE);
			}
		} else if (source == buttonConsultar) {
			platilloBuscar = JOptionPane.showInputDialog(null, "Nombre del platillo a buscar", "Buscar platillo",
					JOptionPane.INFORMATION_MESSAGE);
			if (platilloBuscar != null) {
				rs = recibir.consultarPlatillo(platilloBuscar);
				try {
					if (rs.next()) {
						nombre.setText(rs.getString(2));
						tipoPlatillo.setSelectedItem(rs.getString(3));
						tipoPreparacion.setSelectedItem(rs.getString(4));
						tipoComida.setSelectedItem(rs.getString(5));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else if (source == buttonModificar) {
			recibir.modificarPlatillo(nombre.getText(), String.valueOf(tipoPreparacion.getSelectedItem()));
			JOptionPane.showMessageDialog(null, "Platillo modificado", "Modificaci�n de platillo",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (source == buttonEliminar) {
			int respuesta = JOptionPane.showConfirmDialog(null, "�Est�s seguro de querer eliminar dicho platillo?",
					"Eliminando platillo", JOptionPane.YES_NO_OPTION);
			if (respuesta == 0) {
				JOptionPane.showMessageDialog(null, "Platillo eliminado", "Eliminando platillo",
						JOptionPane.INFORMATION_MESSAGE);
				recibir.eliminarPlatillo(nombre.getText());
			} else {
				JOptionPane.showMessageDialog(null, "Cancelando proceso", "Proceso cancelado",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	public void rellenarCampos(String nombrePlatillo, String tipoPlatillo, String tipoPreparacion, String tipoComida) {
		// nombre.setText(nombrePlatillo);
		JOptionPane.showMessageDialog(null, "Rellenar campos." + nombrePlatillo);
		// setRespaldo(nombrePlatillo);
		// nombre.setText(getRespaldo());
	}

}
