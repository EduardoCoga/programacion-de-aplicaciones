package GUIs;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

public class GUI implements ActionListener {

	JFrame framePrincipal;
	Dimension dim;

	static JDesktopPane desktopPane;
	
	JMenuBar mbBarra;

	JMenu menuInicio;
	JMenuItem miExit;

	JMenu menuAdministrativo;
	JMenuItem abrirPlatillo;
	JMenuItem abrirIngrediente;
	JMenuItem abrirRecetas;
	
	JMenu menuReportes;
	JMenuItem abrirReportePlatillo;
	JMenuItem abrirReporteIngrediente;
	JMenuItem abrirReporteRecetas;
	
	JLabel bienvenida;
	

	public void createGUI() {
		// Ventana principal
		framePrincipal = new JFrame("Platillos UTTEC");
		dim = framePrincipal.getToolkit().getScreenSize();
		framePrincipal.setSize(dim);
		framePrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		

		// Desktop Pane
		bienvenida = new JLabel("Bienvenido a las mejores recetas de platillos...");
		bienvenida.setFont(new Font("Bodoni MT Black", 3, 40));
		desktopPane = new JDesktopPane();		
		desktopPane.setBackground(new Color(60, 90, 153));
		// desktopPane.setSize(900,600);
		framePrincipal.getContentPane().add(desktopPane);		


		// Creacion de botones de menu
		miExit = new JMenuItem("Salir");
		miExit.addActionListener(this);

		// Creacion del menu inicio
		menuInicio = new JMenu("Inicio");
		menuInicio.add(miExit);

		// Creacion del menu administrativo
		menuAdministrativo = new JMenu("Administración");
		abrirPlatillo = new JMenuItem("Platillos");
		abrirIngrediente = new JMenuItem("Ingredientes");
		abrirRecetas = new JMenuItem("Recetas");
		abrirPlatillo.addActionListener(this);
		abrirIngrediente.addActionListener(this);
		abrirRecetas.addActionListener(this);
		menuAdministrativo.add(abrirPlatillo);
		menuAdministrativo.add(abrirIngrediente);
		menuAdministrativo.add(abrirRecetas);
		
		//Creación del menu Reportes
		menuReportes = new JMenu("Generar reportes");
		abrirReportePlatillo = new JMenuItem("Reporte platillos");
		abrirReporteIngrediente = new JMenuItem("Reporte ingredientes");
		abrirReporteRecetas = new JMenuItem("Reporte recetas");
		abrirReporteIngrediente.addActionListener(this);
		abrirReportePlatillo.addActionListener(this);
		abrirReporteRecetas.addActionListener(this);
		menuReportes.add(abrirReporteIngrediente);
		menuReportes.add(abrirReportePlatillo);
		menuReportes.add(abrirReporteRecetas);

		// Creación de la barra de menu
		mbBarra = new JMenuBar();
		mbBarra.add(menuInicio);
		mbBarra.add(menuAdministrativo);
		mbBarra.add(menuReportes);

		// Agregar a framePrincipal
		framePrincipal.setJMenuBar(mbBarra);
		framePrincipal.setVisible(true);
	}

	


	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		Object source = evt.getSource();
		if (source == miExit) {
			System.exit(0);
		} else if (source == abrirPlatillo) {			
			//crearFrameIngrediente();
			GUIPlatillo objPla = new GUIPlatillo();
			objPla.crearFramePlatillo();
		} else if (source == abrirIngrediente) {
			GUIIngredientes objIng = new GUIIngredientes();
			objIng.crearFrameIngrediente();
		} else if(source == abrirRecetas){
			GUIRecetas objRec = new GUIRecetas();
			objRec.crarFrameRecetas();
		} else if (source == abrirReportePlatillo){
			GUIReportePlatillos objRePla = new GUIReportePlatillos();
			objRePla.crearFrameReportesPlatillos();			
		} else if(source == abrirReporteIngrediente){
			GUIReporteIngredientes objReIng = new GUIReporteIngredientes();
			objReIng.crearFrameReportesIngrediente();
		} else if(source == abrirReporteRecetas){
			GUIReporteRecetas objReRec = new GUIReporteRecetas();
			objReRec.crearFrameReportesRecetas();
		}
	}
	
}
