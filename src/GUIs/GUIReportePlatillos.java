package GUIs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

public class GUIReportePlatillos extends GUI{
	JInternalFrame frameReportePlatillos;
	JPanel panelInformativoReportePlatillos;
	JPanel panelInputsReportesPlatillos;
	JPanel panelControlReportesPlatillos;
	JButton buttonGenerarReportePlatillos;

	public void crearFrameReportesPlatillos() {
		// JInternalFrame
		Font fuente = new Font("Arial", 3, 20);
		JComboBox<String> tipoFiltro;
		JLabel info;

		// InternalFrame Platillos
		frameReportePlatillos = new JInternalFrame("Reporte Platillos", true, true, true);
		frameReportePlatillos.setBounds(0, 0, 700, 400);
		frameReportePlatillos.setLayout(new FlowLayout(FlowLayout.CENTER));
		frameReportePlatillos.setBackground(new Color(60, 90, 153));
		// framePlatillos.setLayout(new FlowLayout());

		//panel inforamtivo
		panelInformativoReportePlatillos = new JPanel();
		panelInformativoReportePlatillos.setBackground(new Color(60, 90, 153));		
		panelInformativoReportePlatillos.setLayout(new FlowLayout());
		//Informaci�n del frame
		info = new JLabel("En esta secci�n podr� generar reportes de acuerdo a sus necesidades");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		panelInformativoReportePlatillos.add(info);	
					
		
		// Panel de entrada
		panelInputsReportesPlatillos = new JPanel();		
		panelInputsReportesPlatillos.setBackground(new Color(60, 90, 153));
		panelInputsReportesPlatillos.setLayout(new GridLayout(2, 1));
		
		// Primer dato
		info = new JLabel();
		info.setText("Filtro: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));				
		tipoFiltro = new JComboBox<String>();
		tipoFiltro.addItem("Cucharada");
		tipoFiltro.addItem("Gramos");
		tipoFiltro.addItem("Kgs.");
		tipoFiltro.addItem("Lts.");
		tipoFiltro.addItem("Ml.");
		tipoFiltro.setBackground(new Color(255, 255, 255));
		tipoFiltro.setForeground(new Color(0, 0, 0));
		panelInputsReportesPlatillos.add(info);	
		panelInputsReportesPlatillos.add(tipoFiltro);

		panelInputsReportesPlatillos.setPreferredSize(new Dimension(500, 200));

		// Panel de control
		panelControlReportesPlatillos = new JPanel();
		panelControlReportesPlatillos.setFont(fuente);
		panelControlReportesPlatillos.setBackground(new Color(60, 90, 153));		
		panelControlReportesPlatillos.setPreferredSize(new Dimension(500, 100));
		panelControlReportesPlatillos.setLayout(new FlowLayout(SwingConstants.VERTICAL, 10, 10));

		// creaci�nn de botones
		buttonGenerarReportePlatillos = new JButton("Generar PDF");

		// Tipo de fuente
		buttonGenerarReportePlatillos.setFont(fuente);
		buttonGenerarReportePlatillos.setBackground(new Color(255, 255, 255));
		buttonGenerarReportePlatillos.setForeground(new Color(0, 0, 0));
		buttonGenerarReportePlatillos.setFont(fuente);
		
		// ActionListener
		buttonGenerarReportePlatillos.addActionListener(this);

		// Agregar al panel control
		panelControlReportesPlatillos.add(buttonGenerarReportePlatillos);

		// Agregar al DesktopPane
		frameReportePlatillos.add(panelInformativoReportePlatillos);
		frameReportePlatillos.add(panelInputsReportesPlatillos);
		frameReportePlatillos.add(panelControlReportesPlatillos);
		frameReportePlatillos.setMinimumSize(new Dimension(690,330));
		frameReportePlatillos.setMaximumSize(new Dimension(1100,410));
		desktopPane.add(frameReportePlatillos);
		frameReportePlatillos.show();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		Object source = evt.getSource();
		if (source == buttonGenerarReportePlatillos) {
			System.out.println("Generar Reporte Platillos");
		}
	}
}
