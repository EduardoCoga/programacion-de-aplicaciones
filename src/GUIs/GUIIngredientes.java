package GUIs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import instrucciones.Ingrediente;

public class GUIIngredientes extends GUI {

	JInternalFrame frameIngredientes;
	JPanel panelInputsIngredientes;
	JPanel panelControlIngredientes;
	JButton buttonAgregarIngredientes;
	JButton buttonEliminarIngredientes;
	JButton buttonModificarIngredientes;
	JButton buttonConsultarIngredientes;

	JTextField nombreIngrediente;
	JSpinner cantidad;
	JComboBox<String> tipoCantidad;
	JComboBox<String> tipoIngrediente;

	public void crearFrameIngrediente() {
		// JInternalFrame
		Font fuente = new Font("Arial", 3, 20);
		JLabel info;

		// InternalFrame Ingredientes
		frameIngredientes = new JInternalFrame("Ingredientes", true, true, true);
		frameIngredientes.setBounds(0, 0, 700, 400);
		frameIngredientes.setLayout(new FlowLayout(FlowLayout.CENTER));
		frameIngredientes.setBackground(new Color(60, 90, 153));
		// framePlatillos.setLayout(new FlowLayout());

		// Panel de entrada
		panelInputsIngredientes = new JPanel();
		panelInputsIngredientes.setBackground(new Color(60, 90, 153));
		panelInputsIngredientes.setLayout(new GridLayout(4, 2));

		// Primer dato
		info = new JLabel();
		info.setText("Ingrediente: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		nombreIngrediente = new JTextField();
		nombreIngrediente.setFont(fuente);
		nombreIngrediente.setBackground(new Color(255, 255, 255));
		nombreIngrediente.setForeground(new Color(0, 0, 0));
		panelInputsIngredientes.add(info);
		panelInputsIngredientes.add(nombreIngrediente);

		// Segundo Dato
		info = new JLabel("Cantidad: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		SpinnerModel sm = new SpinnerNumberModel(0, 0, 1000, 1);
		cantidad = new JSpinner(sm);
		cantidad.setFont(fuente);
		cantidad.setBackground(new Color(255, 255, 255));
		cantidad.setForeground(new Color(0, 0, 0));
		panelInputsIngredientes.add(info);
		panelInputsIngredientes.add(cantidad);

		// Tercer Dato
		info = new JLabel("Tipo de cantidad: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		tipoCantidad = new JComboBox<String>();
		tipoCantidad.addItem("Cucharada");
		tipoCantidad.addItem("Gramos");
		tipoCantidad.addItem("Kgs.");
		tipoCantidad.addItem("Lts.");
		tipoCantidad.addItem("Ml.");
		tipoCantidad.setBackground(new Color(255, 255, 255));
		tipoCantidad.setForeground(new Color(0, 0, 0));
		tipoCantidad.setFont(fuente);
		panelInputsIngredientes.add(info);
		panelInputsIngredientes.add(tipoCantidad);

		// Cuarto dato
		info = new JLabel("Tipo de ingrediente: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		tipoIngrediente = new JComboBox<>();
		tipoIngrediente.addItem("Especias");
		tipoIngrediente.addItem("Aceites");
		tipoIngrediente.addItem("Conservas Y Deshidratados");
		tipoIngrediente.addItem("Salsas");
		tipoIngrediente.addItem("Hierbas");
		tipoIngrediente.addItem("Pastas");
		tipoIngrediente.addItem("Legumbres");	
		tipoIngrediente.setBackground(new Color(255, 255, 255));
		tipoIngrediente.setForeground(new Color(0, 0, 0));
		tipoIngrediente.setFont(fuente);
		panelInputsIngredientes.add(info);
		panelInputsIngredientes.add(tipoIngrediente);

		panelInputsIngredientes.setPreferredSize(new Dimension(500, 200));

		// Panel de control
		panelControlIngredientes = new JPanel();
		panelControlIngredientes.setFont(fuente);
		panelControlIngredientes.setBackground(new Color(60, 90, 153));
		panelControlIngredientes.setPreferredSize(new Dimension(500, 100));
		panelControlIngredientes.setLayout(new FlowLayout(SwingConstants.VERTICAL, 10, 10));

		// creaci�nn de botones
		buttonAgregarIngredientes = new JButton("Agregar");
		buttonConsultarIngredientes = new JButton("Consultar");
		buttonEliminarIngredientes = new JButton("Eliminar");
		buttonModificarIngredientes = new JButton("Modificar");

		// Tipo de fuente
		buttonAgregarIngredientes.setFont(fuente);
		buttonAgregarIngredientes.setBackground(new Color(255, 255, 255));
		buttonAgregarIngredientes.setForeground(new Color(0, 0, 0));
		buttonConsultarIngredientes.setFont(fuente);
		buttonConsultarIngredientes.setBackground(new Color(255, 255, 255));
		buttonConsultarIngredientes.setForeground(new Color(0, 0, 0));
		buttonModificarIngredientes.setFont(fuente);
		buttonModificarIngredientes.setBackground(new Color(255, 255, 255));
		buttonModificarIngredientes.setForeground(new Color(0, 0, 0));
		buttonEliminarIngredientes.setFont(fuente);
		buttonEliminarIngredientes.setBackground(new Color(255, 255, 255));
		buttonEliminarIngredientes.setForeground(new Color(0, 0, 0));

		// ActionListener
		buttonAgregarIngredientes.addActionListener(this);
		buttonConsultarIngredientes.addActionListener(this);
		buttonModificarIngredientes.addActionListener(this);
		buttonEliminarIngredientes.addActionListener(this);

		// Agregar al panel control
		panelControlIngredientes.add(buttonAgregarIngredientes);
		panelControlIngredientes.add(buttonConsultarIngredientes);
		panelControlIngredientes.add(buttonModificarIngredientes);
		panelControlIngredientes.add(buttonEliminarIngredientes);

		// Agregar al DesktopPane
		frameIngredientes.add(panelInputsIngredientes);
		frameIngredientes.add(panelControlIngredientes);
		//Alturas minimas y maximas
		frameIngredientes.setMinimumSize(new Dimension(700,350));
		frameIngredientes.setMaximumSize(new Dimension(1100, 400));
		desktopPane.add(frameIngredientes);
		frameIngredientes.show();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		Object source = evt.getSource();
		Ingrediente ejecutar = new Ingrediente();
		String ingredienteBuscado;
		ResultSet rs;

		if (source == buttonAgregarIngredientes) {

			ejecutar.registrarIngrediente(nombreIngrediente.getText(),
					Integer.parseInt(String.valueOf(cantidad.getValue())),
					String.valueOf(tipoCantidad.getSelectedItem()), String.valueOf(tipoIngrediente.getSelectedItem()));
		} else if (source == buttonConsultarIngredientes) {
			ingredienteBuscado = JOptionPane.showInputDialog(null, "Nombre del ingrediente a buscar",
					"Buscar ingrediente", JOptionPane.INFORMATION_MESSAGE);
			if (ingredienteBuscado != null) {
				rs = ejecutar.consultarIngrediente(ingredienteBuscado);
				try {
					if (rs.next()) {
						nombreIngrediente.setText(rs.getString(2));
						cantidad.setValue(rs.getInt(3));
						tipoCantidad.setSelectedItem(rs.getString(4));
						tipoIngrediente.setSelectedItem(rs.getString(5));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else if (source == buttonModificarIngredientes) {
			ejecutar.modificarIngrediente(nombreIngrediente.getText(),
					Integer.parseInt(String.valueOf(cantidad.getValue())));
			JOptionPane.showMessageDialog(null, "Ingrediente modificado", "Modificaci�n de ingrediente",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (source == buttonEliminarIngredientes) {
			int respuesta = JOptionPane.showConfirmDialog(null, "�Est�s seguro de querer eliminar dicho ingrediente?",
					"Eliminando ingrediente", JOptionPane.YES_NO_OPTION);
			if (respuesta == 0) {
				JOptionPane.showMessageDialog(null, "ingrediente eliminado", "Eliminando ",
						JOptionPane.INFORMATION_MESSAGE);
				ejecutar.eliminarIngrediente(nombreIngrediente.getText());
			} else {
				JOptionPane.showMessageDialog(null, "Cancelando proceso", "Proceso cancelado",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
}
