package GUIs;

import java.awt.Color;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import baseDeDatos.Conexion;
import baseDeDatos.ConexionSingleton;
import instrucciones.Receta;
import observer.ObjetoAccionador;
import observer.Objeto;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;

public class GUIRecetas extends GUI {
	String query;
	ResultSet resultado = null;
	PreparedStatement statement = null;
	Connection conn = ConexionSingleton.getInstance();
	JInternalFrame frameRecetas;
	JPanel panelInputsRecetas;
	JPanel panelControlRecetas;
	JButton buttonAgregarRecetas;
	//JButton buttonEliminarRecetas;
	//JButton buttonModificarRecetas;
	JButton buttonConsultarRecetas;

	JTextField nombreReceta;
	JComboBox<String> nombrePlatillo;
	// JList
	JList listaNombres;
	DefaultListModel modelo;// declaramos el Modelo
	JScrollPane scrollLista;
	JLabel info;

	public void crarFrameRecetas() {

		// JInternalFrame
		Font fuente = new Font("Arial", 3, 20);

		// InternalFrame Platillos
		frameRecetas = new JInternalFrame("Recetas", true, true, true);
		frameRecetas.setBounds(0, 0, 700, 500);
		frameRecetas.setLayout(new FlowLayout(FlowLayout.CENTER));
		frameRecetas.setBackground(new Color(60, 90, 153));
		// framePlatillos.setLayout(new FlowLayout());

		// Panel de entrada
		panelInputsRecetas = new JPanel();
		panelInputsRecetas.setBackground(new Color(60, 90, 153));
		panelInputsRecetas.setLayout(new GridLayout(4, 1));

		// Primer Dato
		info = new JLabel();
		info.setText("Nombre de la receta: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		panelInputsRecetas.add(info);
		nombreReceta = new JTextField();
		nombreReceta.setFont(fuente);
		nombreReceta.setBackground(new Color(255, 255, 255));
		nombreReceta.setForeground(new Color(0, 0, 0));
		panelInputsRecetas.add(info);
		panelInputsRecetas.add(nombreReceta);

		// Segundo Dato
		info = new JLabel();
		info.setText("Nombre del platillo: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		panelInputsRecetas.add(info);
		nombrePlatillo = new JComboBox<String>();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM platillos";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			while (resultado.next()) {
				nombrePlatillo.addItem(resultado.getString(2));
			}
		} catch (Exception e) {
			System.out.print(e);
		}
		nombrePlatillo.setBackground(new Color(255, 255, 255));
		nombrePlatillo.setForeground(new Color(0, 0, 0));
		nombrePlatillo.setFont(fuente);
		panelInputsRecetas.add(nombrePlatillo);

		// Tercer Dato
		info = new JLabel();
		info.setText("Ingrediente: ");
		info.setFont(fuente);
		info.setForeground(new Color(255, 255, 255));
		panelInputsRecetas.add(info);

		// instanciamos la lista
		listaNombres = new JList();
		listaNombres.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// instanciamos el modelo
		modelo = new DefaultListModel();
		// instanciamos el Scroll que tendra la lista
		scrollLista = new JScrollPane(listaNombres);
		// scrollLista.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollLista.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		try {
			query = "SELECT * FROM ingredientes";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();			
			while (resultado.next()) {
				modelo.addElement(resultado.getString(2));
				listaNombres.setModel(modelo);
			}			
									
		} catch (Exception e) {
			System.out.print(e);
		}
		listaNombres.setBackground(new Color(255, 255, 255));
		listaNombres.setForeground(new Color(0, 0, 0));
		listaNombres.setFont(fuente);
		panelInputsRecetas.add(scrollLista);

		panelInputsRecetas.setPreferredSize(new Dimension(500, 400));

		// Panel de control
		panelControlRecetas = new JPanel();
		panelControlRecetas.setFont(fuente);
		panelControlRecetas.setBackground(new Color(60, 90, 153));
		panelControlRecetas.setPreferredSize(new Dimension(500, 100));
		panelControlRecetas.setLayout(new FlowLayout(SwingConstants.VERTICAL, 10, 10));

		// Crear botones
		buttonAgregarRecetas = new JButton("Agregar");
		buttonConsultarRecetas = new JButton("Consultar");
		//buttonEliminarRecetas = new JButton("Eliminar");
		//buttonModificarRecetas = new JButton("Modificar");

		// Tipo de fuente y color de los BOTONES
		buttonAgregarRecetas.setFont(fuente);
		buttonAgregarRecetas.setBackground(new Color(255, 255, 255));
		buttonAgregarRecetas.setForeground(new Color(0, 0, 0));
		buttonConsultarRecetas.setFont(fuente);
		buttonConsultarRecetas.setBackground(new Color(255, 255, 255));
		buttonConsultarRecetas.setForeground(new Color(0, 0, 0));
		/*buttonModificarRecetas.setFont(fuente);
		buttonModificarRecetas.setBackground(new Color(255, 255, 255));
		buttonModificarRecetas.setForeground(new Color(0, 0, 0));
		buttonEliminarRecetas.setFont(fuente);
		buttonEliminarRecetas.setForeground(new Color(0, 0, 0));
		buttonEliminarRecetas.setBackground(new Color(255, 255, 255));*/

		// Action listener
		buttonAgregarRecetas.addActionListener(this);
		buttonConsultarRecetas.addActionListener(this);

		// Agregar al panel
		panelControlRecetas.add(buttonAgregarRecetas);
		panelControlRecetas.add(buttonConsultarRecetas);
		//panelControlRecetas.add(buttonModificarRecetas);
		//panelControlRecetas.add(buttonEliminarRecetas);

		// Agregar al DesktopPane
		frameRecetas.add(panelInputsRecetas);
		frameRecetas.add(panelControlRecetas);
		//Alturas minimas y maximas
		frameRecetas.setMinimumSize(new Dimension(620,450));
		frameRecetas.setMaximumSize(new Dimension(1100, 550));
		desktopPane.add(frameRecetas);
		frameRecetas.show();
	}

	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		//conn = ConexionSingleton.getInstance();
		Receta ejecutar = new Receta();
		Object source = evt.getSource();
		PreparedStatement statement = null;
		String recetaBuscar;
		if (source == buttonAgregarRecetas) {
			//String.valueOf(nombrePlatillo.getSelectedItem());
			if(nombreReceta.getText().isEmpty()){				
				JOptionPane.showMessageDialog(null, "Ingrese los datos solicitados", "Error",
						JOptionPane.ERROR_MESSAGE);
			}else{			
				/*Objeto obj = new Objeto();
				ObjetoAccionador a = new ObjetoAccionador();
				a.enlazarObservador(obj);
				a.realizarAccion();*/
				ejecutar.registrarReceta(nombreReceta.getText(), String.valueOf(nombrePlatillo.getSelectedItem()),
					listaNombres.getSelectedValuesList().toArray());
			}
			
		} else if (source == buttonConsultarRecetas) {
			recetaBuscar = JOptionPane.showInputDialog(null, "Nombre del platillo a buscar", "Buscar platillo",
					JOptionPane.INFORMATION_MESSAGE);
			if (recetaBuscar != null) {
				resultado = ejecutar.consultarReceta(recetaBuscar);
				try {
					if(resultado.next()){
						nombreReceta.setText(resultado.getString(3));
						nombrePlatillo.setSelectedItem(resultado.getString(1));
						resultado.beforeFirst();
						int indice;
						while (resultado.next()) {
							//System.out.println("Ingrediente: " + resultado.getString(2));
							for(indice = 0; indice < modelo.size(); indice++)
								System.out.println(indice);
								
								//listaNombres.setSelectedValue(resultado.getString(2),true);
							//listaNombres.getSelectionModel().setSelectionMode();							
							//listaNombres.getSelectionModel().setSelectionInterval(0, 0);
						}
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
