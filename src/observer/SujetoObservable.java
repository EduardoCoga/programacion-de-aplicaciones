package observer;

public interface SujetoObservable {

	public void notificar();//avisa cuanda el evento es accionado
}
