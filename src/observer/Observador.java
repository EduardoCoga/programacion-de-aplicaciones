package observer;

public interface Observador {

	public void update();//Se actualiza cuando el sujeto lo notifique o el evento se desata

	/*public static void main(String[] args){
		Objeto obj = new Objeto();
		ObjetoAccionador objA = new ObjetoAccionador();
		objA.enlazarObservador(obj);
		objA.realizarAccion();
	}*/
}
