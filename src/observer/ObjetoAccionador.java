package observer;

import java.util.ArrayList;

public class ObjetoAccionador implements SujetoObservable {

	private ArrayList<Observador>observadores;
	
	public ObjetoAccionador(){
		observadores = new ArrayList<Observador>();
	}
	
	public void realizarAccion(){
		notificar();
	}
	
	public void enlazarObservador(Observador o ){
		observadores.add(o);
	}
	
	@Override
	public void notificar() {
		// TODO Auto-generated method stub
		for(Observador o : observadores){
			o.update();
		}
	}

}
