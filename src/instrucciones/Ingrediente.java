package instrucciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import baseDeDatos.ConexionSingleton;

public class Ingrediente {
	String query;
	ResultSet resultado = null;
	PreparedStatement statement = null;
	Connection conn = ConexionSingleton.getInstance();

	public void registrarIngrediente(String nombreIngrediente, int cantidad, String tipoCantidad,
			String tipoIngrediente) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM ingredientes WHERE nombre_ingrediente = '" + nombreIngrediente + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			if (resultado.next()) {
				System.out.println("Ese ingrediente ya existe.");
			} else {
				System.out.println("Entra a else.");
				query = "INSERT INTO ingredientes VALUES(null,'" + nombreIngrediente + "','" + cantidad + "'," + "'"
						+ tipoCantidad + "','" + tipoIngrediente + "')";
				statement = conn.prepareStatement(query);
				int verificarQuery = statement.executeUpdate();
				if (verificarQuery > 0) {
					System.out.println("Ingrediente registrado");
					conn.close();
				} else {
					System.out.println("Error al registrar ingrediente.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public ResultSet consultarIngrediente(String ingredienteBuscar) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM ingredientes WHERE nombre_ingrediente= '" + ingredienteBuscar + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			conn.close();
		} catch (Exception e) {
			System.out.print(e);
		}
		return resultado;
	}

	public void modificarIngrediente(String nombreIngrediente, int cantidadIngrediente) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM ingredientes WHERE nombre_ingrediente = '" + nombreIngrediente + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			if (resultado.next()) {
				query = "UPDATE ingredientes SET cantidad_ingrediente = '" + cantidadIngrediente
						+ "' WHERE nombre_ingrediente = '" + nombreIngrediente + "'";
				statement = conn.prepareStatement(query);
				int verificarQuery = statement.executeUpdate();
				if (verificarQuery > 0) {
					System.out.println("Ingrediente modificado");
					conn.close();
				} else {
					System.out.println("Error al modificar ingrediente.");
				}
			} else {
				System.out.println("Error");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public void eliminarIngrediente(String nombreIngrediente) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM ingredientes WHERE nombre_ingrediente = '" + nombreIngrediente + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			if (resultado.next()) {
				query = "DELETE FROM ingredientes WHERE nombre_ingrediente = '" + nombreIngrediente + "'";
				statement = conn.prepareStatement(query);
				int verificarQuery = statement.executeUpdate();
				if (verificarQuery > 0) {
					System.out.println("Ingrediente eliminado");
				} else {
					System.out.println("Error al eliminar ingrediente.");
				}
			} else {
				System.out.println("Error");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
}
