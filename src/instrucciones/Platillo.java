package instrucciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import baseDeDatos.ConexionSingleton;

public class Platillo {
	String query;
	ResultSet resultado = null;
	PreparedStatement statement = null;
	Connection conn = ConexionSingleton.getInstance();
	int i;

	public ResultSet consultarPlatillo(String platilloBuscar) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM platillos WHERE nombre_platillo = '" + platilloBuscar + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			conn.close();
		} catch (Exception e) {
			System.out.print(e);
		}
		return resultado;
	}

	public void modificarPlatillo(String nombrePlatillo, String tipoPreparacion) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM platillos WHERE nombre_platillo = '" + nombrePlatillo + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			if (resultado.next()) {
				query = "UPDATE platillos SET nombre_platillo = '" + nombrePlatillo + "',tipo_preparacion= '"
						+ tipoPreparacion + "' WHERE nombre_platillo = '" + nombrePlatillo + "'";
				statement = conn.prepareStatement(query);
				int verificarQuery = statement.executeUpdate();
				if (verificarQuery > 0) {
					System.out.println("Platillo modificado");
					conn.close();
				} else {
					System.out.println("Error al registrar platillo.");
				}
			} else {
				System.out.println("Error");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public void eliminarPlatillo(String nombrePlatillo) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM platillos WHERE nombre_platillo = '" + nombrePlatillo + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			if (resultado.next()) {
				query = "DELETE FROM platillos WHERE nombre_platillo = '" + nombrePlatillo + "'";
				statement = conn.prepareStatement(query);
				int verificarQuery = statement.executeUpdate();
				if (verificarQuery > 0) {
					System.out.println("Platillo eliminado");
					conn.close();
				} else {
					System.out.println("Error al eliminar platillo.");
				}
			} else {
				System.out.println("Error");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
}
