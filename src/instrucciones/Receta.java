package instrucciones;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.List;
import java.sql.Connection;
import java.sql.SQLException;

import baseDeDatos.Conexion;
import baseDeDatos.ConexionSingleton;

public class Receta {
	Connection conn = ConexionSingleton.getInstance();
	String query;
	ResultSet resultado = null;
	PreparedStatement statement = null;
	int id_platillo;
	int id_ingrediente;

	public void registrarReceta(String nombreReceta, String nombrePlatillo, Object[] ingredientes) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM registro_recetas WHERE nombre_receta = '" + nombreReceta + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			if (resultado.next()) {
				System.out.println("Esa receta ya existe.");
			} else {
				query = "SELECT id_platillo FROM platillos WHERE nombre_platillo = '" + nombrePlatillo + "'";
				statement = conn.prepareStatement(query);
				resultado = statement.executeQuery();
				if (resultado.next()) {
					id_platillo = resultado.getInt(1);
				}
				System.out.println("Ingredientes " + ingredientes);
				for (int i = 0; i < ingredientes.length; i++) {
					query = "SELECT id_ingrediente FROM ingredientes WHERE nombre_ingrediente = '" + ingredientes[i]
							+ "'";
					statement = conn.prepareStatement(query);
					resultado = statement.executeQuery();
					if (resultado.next()) {
						id_ingrediente = resultado.getInt(1);
						System.out.println("Ingredientes " + id_ingrediente);
						query = "INSERT INTO registro_recetas VALUES(null,'" + nombreReceta + "','" + id_platillo + "',"
								+ "'" + id_ingrediente + "')";
						statement = conn.prepareStatement(query);
						int verificarQuery = statement.executeUpdate();
						if (verificarQuery > 0) {
							System.out.println("Receta registrada");
						} else {
							System.out.println("Error al registrar receta.");
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public ResultSet consultarReceta(String nombreReceta) {
		conn = ConexionSingleton.getInstance();
		PreparedStatement statement = null;
		try {
			//System.out.println(nombreReceta);
			query = "SELECT nombre_platillo, nombre_ingrediente, nombre_receta FROM registro_recetas"
					+ " INNER JOIN platillos ON registro_recetas.id_platillo=platillos.id_platillo"
					+ " INNER JOIN ingredientes ON registro_recetas.id_ingrediente=ingredientes.id_ingrediente"
					+ " WHERE nombre_receta = '" + nombreReceta + "'";
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
		} catch (Exception e) {
			System.out.print(e);
		}
		return resultado;
	}
}
