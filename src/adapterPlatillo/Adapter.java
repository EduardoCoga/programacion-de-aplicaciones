package adapterPlatillo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JOptionPane;

import baseDeDatos.ConexionSingleton;

public class Adapter extends TargetPlatillo {

	String nombre;
	String tipo;
	String tipoPreparacion;
	String tipoComida;
	Desayuno desayuno;
	String query;
	ResultSet resultado = null;
	PreparedStatement statement = null;
	Connection conn = ConexionSingleton.getInstance();
	int i;

	@Override
	public void setNombre(String nombre) {
		// TODO Auto-generated method stub
		this.nombre = nombre;
	}

	@Override
	public String getNombre() {
		// TODO Auto-generated method stub
		return nombre;
	}

	@Override
	public void setTipo(String tipo) {
		// TODO Auto-generated method stub
		this.tipo = tipo;
	}

	@Override
	public String getTipo() {
		// TODO Auto-generated method stub
		return tipo;
	}

	@Override
	public void setTipoPreparacion(String tipoPreparacion) {
		// TODO Auto-generated method stub
		this.tipoPreparacion = tipoPreparacion;
	}

	@Override
	public String getTipoPreparacion() {
		// TODO Auto-generated method stub
		return tipoPreparacion;
	}

	@Override
	public void setTipoComida(String tipoComida) {
		// TODO Auto-generated method stub
		this.tipoComida = tipoComida;
	}

	@Override
	public String getTipoComida() {
		// TODO Auto-generated method stub
		return tipoComida;
	}

	public void registrarPlatillo(String nombrePlatillo, String tipoPlatillo, String tipoPreparacion,
			String tipoComida) {				
		PreparedStatement statement = null;
		try {
			query = "SELECT * FROM platillos WHERE nombre_platillo = '" + nombrePlatillo + "'";
			//statement = conexion.prepareStatement(query);
			statement = conn.prepareStatement(query);
			resultado = statement.executeQuery();
			if (resultado.next()) {
				i = 0;
				System.out.println("Ese platillo ya existe.");
			} else {
				desayuno = new Desayuno(true);
				query = "INSERT INTO platillos VALUES(null,'" + nombrePlatillo + "','" + tipoPlatillo + "'," + "'"
						+ tipoPreparacion + "','" + tipoComida + "'," + desayuno.isDesayuno() + ")";
				statement = conn.prepareStatement(query);
				int verificarQuery = statement.executeUpdate();
				i = 1;
				if (verificarQuery > 0) {
					System.out.println("Platillo registrado");					
					JOptionPane.showMessageDialog(null, "Registro exitoso", "Exito", JOptionPane.INFORMATION_MESSAGE);
					/*GUIPlatillo pla = new GUIPlatillo();
					pla.limpiarCampos();*/
					conn.close();
				} else {
					System.out.println("Error al registrar platillo.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
}
