package adapterPlatillo;

public abstract class TargetPlatillo {
	
	
	public abstract void setNombre(String nombre);
	public abstract String getNombre();

	public abstract void setTipo(String tipo);
	public abstract String getTipo();
	
	public abstract void setTipoPreparacion(String tipoPreparacion);
	public abstract String getTipoPreparacion();
	
	public abstract void setTipoComida(String tipoComida);
	public abstract String getTipoComida();
	
	public abstract void registrarPlatillo(String nombrePlatillo, String tipoPlatillo, String tipoPreparacion,
			String tipoComida);
}
